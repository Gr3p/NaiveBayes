const Promise = require('bluebird');
const NaiveBayes = require('./lib/NaiveBayes');

let classifier = new NaiveBayes();
let STRING_TO_TEST = 'Buongiorno, vorrei avere un caffe!';
let trainingSet = [
    {text: 'ciao bello', category: 'saluto'},
    {text: 'buongiorno', category: 'saluto'},
    {text: 'come stai?', category: 'saluto'},
    {text: 'ciao', category: 'saluto'},
    {text: 'buonasera', category: 'saluto'},
    {text: 'addio', category: 'saluto'},
    {text: 'Buongiorno signori e signore', category: 'saluto'},
    {text: 'maledizione!!', category: 'imprecazione'},
    {text: 'cazzo!', category: 'imprecazione'},
    {text: 'porco cane!', category: 'imprecazione'},
    {text: 'porco dio!', category: 'imprecazione'}
];

function train() {
    trainingSet.forEach(function (dataObj) {
        classifier.learn(dataObj.text, dataObj.category);
    });
}

function getResult() {
    return classifier.categorize(STRING_TO_TEST);
}


return new Promise.resolve(true)
    .then(train)
    .then(getResult)
    .then(function (res) {
        console.log('TESTING STRING:', STRING_TO_TEST);
        console.log('ACTIVE CATEGORIES:', Object.keys(classifier.categories));
        console.log('RES:', res);
    });