/**
 * This is an ECMAScript6-Extension that I refactor from a scratch to understand Bayes-Algorithm.
 * Many comments come from original code
 *
 * @see https://github.com/ttezel/bayes
 */
module.exports = class NaiveBayes {
    /**
     * This is a Naive-Bayes Classifier that uses Laplace Smoothing.
     * @type {NaiveBayes}
     * @param {Object} options
     * @propriety {Function} tokenizer - Custom callback function to parse STRINGS
     * @propriety {string} localPath - The path of Json-filename or file descriptor
     */
    constructor(options) {
        let self = this;

        // set options object
        self.options = {};
        if (typeof options !== 'undefined') {
            if (!options || typeof options !== 'object' || Array.isArray(options)) {
                throw new Error('NaiveBayes got invalid `options`: `' + options + '`. Pass in an object.');
            }
            self.options = options;
        }

        // keys we use to serialize a classifier's state
        self.STATE_KEYS = [
            'categories', 'docCount', 'totalDocuments', 'vocabulary', 'vocabularySize',
            'wordCount', 'wordFrequencyCount', 'options'
        ];

        self.tokenizer = self.options.tokenizer || self.defaultTokenizer;
        self.localPath = self.options.localPath || 'Bayes_' + new Date().getTime() + '.json';

        //initialize vocabulary and its size
        self.vocabulary = {};
        self.vocabularySize = 0;

        //number of documents we have learned from
        self.totalDocuments = 0;

        //document frequency table for each of categories
        //=> for each category, how often were documents mapped to it
        self.docCount = {};

        //for each category, how many words total were mapped to it
        self.wordCount = {};

        //word frequency table for each category
        //=> for each category, how frequent was a given word mapped to it
        self.wordFrequencyCount = {};

        //category names
        self.categories = {};
    }

    /**
     * Recognizes if running on browser or not.
     * @returns {boolean}
     */
    isBrowser() {
        return new Function('try {return this===window;}catch(e){return false;}')();
    }

    /**
     * Initializes a NaiveBayes instance from a JSON state representation.
     * Use this with classifier.toJson().
     *
     * @param  {String} jsonStr - State representation obtained by classifier.toJson()
     * returns {NaiveBayes} - Naive Bayes Classifier
     */
    fromJson(jsonStr) {
        let self = this;
        let parsed;

        try {
            parsed = JSON.parse(jsonStr);
        } catch (e) {
            throw new Error('Naivebayes.fromJson expects a valid JSON string.');
        }

        // init a new classifier
        let classifier = new NaiveBayes(parsed.options);

        // override the classifier's state
        self.STATE_KEYS.forEach(function (k) {
            if (typeof parsed[k] === 'undefined' || parsed[k] === null) {
                throw new Error('Naivebayes.fromJson: JSON string is missing an expected property: `' + k + '`.');
            }
            classifier[k] = parsed[k];
        });

        return classifier;
    }

    /**
     * Given an input string, tokenize it into an array of word tokens.
     * This is the default parse function used if user does not provide one in `options`.
     *
     * @param  {String} text - The text to normalize
     * returns {Array}
     */
    defaultTokenizer(text) {
        let rgxPunctuation = /[^(a-zA-ZA-Яa-я0-9_)+\s]/g;

        //remove anything from text that isn't a word char or a space
        let sanitized = text.toLowerCase().replace(rgxPunctuation, ' ');

        return sanitized.split(/\s+/);
    }

    /**
     * Initializes each of data structure entries for this new category
     * @param  {String} categoryName
     * @returns {NaiveBayes}
     */
    initializeCategory(categoryName) {
        let self = this;

        if (!self.categories[categoryName]) {
            self.docCount[categoryName] = 0;
            self.wordCount[categoryName] = 0;
            self.wordFrequencyCount[categoryName] = {};
            self.categories[categoryName] = true;
        }
        return self;
    }

    /**
     * Trains naive-bayes classifier by telling it what `category` belongs the given text
     *
     * @param  {String} text - Text to train.
     * @param  {String} category - Category to the `text` belong
     * @returns {NaiveBayes}
     */
    learn(text, category) {
        let self = this;

        //initialize category data structures if we've never seen this category
        self.initializeCategory(category);

        //update count of how many documents mapped to this category
        self.docCount[category]++;

        //update the total number of documents we have learned from
        self.totalDocuments++;

        //normalize the text into a word array
        let tokens = self.tokenizer(text);

        //get a frequency count for each token in the text
        let frequencyTable = self.frequencyTable(tokens);

        /**
         Update vocabulary and word frequency count for this category
         */

        Object
            .keys(frequencyTable)
            .forEach(function (token) {
                //add this word to vocabulary if not already existing
                if (!self.vocabulary[token]) {
                    self.vocabulary[token] = true;
                    self.vocabularySize++;
                }

                let frequencyInText = frequencyTable[token];

                //update the frequency information for this word in this category
                if (!self.wordFrequencyCount[category][token]) {
                    self.wordFrequencyCount[category][token] = frequencyInText;
                } else {
                    self.wordFrequencyCount[category][token] += frequencyInText;
                }

                //update the count of all words we have seen mapped to this category
                self.wordCount[category] += frequencyInText;
            });

        return self;
    }

    /**
     * Determines which trained category `text` belongs to.
     *
     * @param  {String} text
     * returns {String} category
     */
    categorize(text) {
        let self = this;
        let maxProbability = -Infinity;
        let chosenCategory = null;

        let tokens = self.tokenizer(text);
        let frequencyTable = self.frequencyTable(tokens);

        //console.log('FrequencyTable for:', text, '=>', frequencyTable);

        //iterate through categories to find the one with max probability for this text
        Object
            .keys(self.categories)
            .forEach(function (category) {

                //start by calculating the overall probability of this category
                //=>  out of all documents we've ever looked at, how many were
                //    mapped to this category
                let categoryProbability = self.docCount[category] / self.totalDocuments;

                //take the log to avoid underflow
                let logProbability = Math.log(categoryProbability);

                //now determine P( w | c ) for each word `w` in the text
                Object
                    .keys(frequencyTable)
                    .forEach(function (token) {
                        let frequencyInText = frequencyTable[token];
                        let tokenProbability = self.tokenProbability(token, category);

                        //console.log('Token:', token, ' => Category:', category, '=> TokenProbability:', tokenProbability);

                        //determine the log of the P( w | c ) for this word
                        logProbability += frequencyInText * Math.log(tokenProbability);
                    });

                if (logProbability > maxProbability) {
                    maxProbability = logProbability;
                    chosenCategory = category;
                }
            });

        return chosenCategory;
    }

    /**
     * Calculates probability that a `token` belongs to a `category`
     *
     * @param  {String} token
     * @param  {String} category
     * returns {Number} probability
     */
    tokenProbability(token, category) {
        let self = this;

        //how many times this word has occurred in documents mapped to this category
        let wordFrequencyCount = self.wordFrequencyCount[category][token] || 0;

        //what is the count of all words that have ever been mapped to this category
        let wordCount = self.wordCount[category];

        //use laplace Add-1 Smoothing equation
        return ( wordFrequencyCount + 1 ) / ( wordCount + self.vocabularySize );
    }

    /**
     * Builds a frequency property where
     * - the keys are the entries in `tokens`
     * - the values are the frequency of each entry in `tokens`
     *
     * @param  {Array} tokens - Normalized word array
     * @returns {Object}
     */
    frequencyTable(tokens) {
        let frequencyTable = Object.create(null);

        tokens.forEach(function (token) {
            if (!frequencyTable[token]) {
                frequencyTable[token] = 1;
            } else {
                frequencyTable[token]++;
            }
        });

        return frequencyTable;
    }

    /**
     * Dumps the classifier's state as a JSON string.
     * returns {String} - Representation of the classifier.
     */
    toJson() {
        let state = {};
        let self = this;
        self.STATE_KEYS.forEach(function (k) {
            state[k] = self[k];
        });

        return JSON.stringify(state);
    }

    /**
     * Saves dumped classifier's state local as a JSON-File
     * This functions works only on Backend-Environment (Tested on NodeJS - v10.5.0)
     *
     * @param {string} fileName - filename or file descriptor
     * @returns {NaiveBayes}
     */
    saveJsonFile(fileName) {
        let self = this;

        fileName = fileName || self.localPath;
        if (!self.isBrowser()) {
            try {
                require('fs').writeFileSync(fileName, JSON.stringify(JSON.parse(self.toJson())));
                return self;
            }
            catch (err) {
                throw err;
            }
        }
        else {
            throw new Error('The method .saveJson() is not available for Browser Environment');
        }
    }

    /**
     * Gets an already trained distance of NaiveBayes's class.
     * @param {string} fileName - filename or file descriptor
     * @returns {NaiveBayes}
     */
    fromJsonFile(fileName) {
        let self = this;
        //TODO: to test!!
        fileName = fileName || self.localPath;
        if (!self.isBrowser()) {
            try {
                let content = require('fs').readFileSync(fileName);
                if (content && content.length > 0) {
                    return self.fromJson(content);
                }
                else {
                    throw new Error('File ' + fileName + ' is empty');
                }
            }
            catch (err) {
                throw err;
            }
        }
        else {
            throw new Error('The method .saveJson() is not available for Browser Environment');
        }
    }


};