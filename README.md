# Intro


This is an ECMAScript6-Extension that I refactor from a scratch to understand Bayes-Algorithm.
Many comments come from original code.
 
@see https://github.com/ttezel/bayes


This Class works in backend and in frontend too.
You can use it for many applications such as:

- Spam filter
- Classify contents (i.e: technology, politics, or sports?)


Docs come soon...